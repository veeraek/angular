# AngularTourOfHeroes How to run project?

I made this project with Visual Studio Code. So at first,
open the project folder in vs code.

Open new terminal in vs code.

Write to terminal ng serve --open

When opening is done, you can see to project on your web browser.

Tour Of Heroues includes pages Dashboard and Heroes. 

In the dashboard you can see top Heroes, search heroes and see the messages or clear messages.

In the heroes page you can add new heroes, see all of the heroes, delete heroes and also see and clear the heroes.

When you click name of hero, you can see informations about hero. You can also change the name of the hero.

# AngularTourOfHeroes

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.6.
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
